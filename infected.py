#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 10:58:02 2020

@author: judith
"""

from mesa import Agent, Model
from mesa.time import RandomActivation

from mesa_geo.geoagent import GeoAgent, AgentCreator
from mesa_geo import GeoSpace

import math
import networkx as nx
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid
import random
import numpy as np

import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import os
from shapely.geometry import Polygon, LineString, Point
import contextily as ctx

from io import StringIO
import json

import logging

from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer


#PROVA GIT 
#FASFASDFADSF

class BCNCovid2020(Model):
    def __init__(self, N, width, height):
        self.num_agents=N
        self.grid = MultiGrid(width, height, False)
        self.schedule = RandomActivation(self)
        # Create agents
        for i in range(self.num_agents):
            a = A_15(i, self)
            self.schedule.add(a)
            # Add the agent to a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))
    def step(self):
        self.schedule.step()
        
        
class A_15(Agent):
    def __init__(self, unique_id, model, probs=None):
        super().__init__(unique_id, model)
        self.cont_15= 15
        #self.displ_15= 3.5
        self.group=1
        self.state= 'susceptible'
    
    def move(self):
        possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True,include_center=False)
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)
    
    def infection(self, cont_15):
        virus_spread_chance = 0.06
        self.cont_15=cont_15
        for i in range(cont_15):
            prob_infection = virus_spread_chance
            if random.randint(0, 1)< prob_infection:
                cellmates = self.model.grid.get_cell_list_contents([self.pos])
                for i in cellmates:
                    if self.state == 'susceptible':
                        self.state = 'infected'
                
                
    
    def step(self):
        self.move()
        
        if self.state == 'infected':
            self.infection()
            
        

'''class MoneyAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def move(self):
        possible_steps = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=False)
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def give_money(self):
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            other = self.random.choice(cellmates)
            other.wealth += 1
            self.wealth -= 1

    def step(self):
        self.move()
        if self.wealth > 0:
            self.give_money() '''

model = BCNCovid2020(50, 2, 2)
for i in range(20):
    model.step()
    
agent_counts = np.zeros((model.grid.width, model.grid.height))
for cell in model.grid.coord_iter():
    cell_content, x, y = cell
    agent_count = len(cell_content)
    agent_counts[x][y] = agent_count
plt.imshow(agent_counts, interpolation='nearest')
plt.colorbar()


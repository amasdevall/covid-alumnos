from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa_geo.geoagent import GeoAgent, AgentCreator
from mesa_geo import GeoSpace
import math
import networkx as nx
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid
import random
import numpy as np
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import os
from shapely.geometry import Polygon, LineString, Point
import contextily as ctx
from io import StringIO
import json
import logging
_log = logging.getLogger(__name__)





progression_period = 3 #number of days for disease to progress and show symptoms-- mean of a Gaussian distribution

progression_period_asym = 5 #nº days you remain asymptomatic - mean of a Gaussian distribution 

progression_sd = 2 #standard deviation of disease progression -- sd of a Gaussian distribution

progression_sd_asym = 2 #standard deviation of days remaining asymptomatic



#MOVING PROBABILITIES: 

B_OUT= 0.02
L_OUT= 0.096
T_OUT= 0.091
G_OUT= 0.036


B_L=  0.171
B_G= 0.448
B_T= 0.381


L_B= 0.711
L_G= 0.065
L_T= 0.224


G_B= 0.92
G_L= 0.034
G_T= 0.046


T_L= 0.108
T_G= 0.046
T_B= 0.846

#limites de provincias

min_x_B= 1.360378
max_x_B = 2.777757
min_y_B = 41.192688
max_y_B=  42.322241


min_x_T= 0.159413
max_x_T = 1.653270
min_y_T= 40.523825
max_y_T=  41.582856


min_x_G= 1.774888
max_x_G = 3.332542
min_y_G = 41.658425
max_y_G= 42.494258


min_x_L= 0.328102
max_x_L = 1.85532
min_y_L = 41.274248
max_y_L= 42.861497



#List of memory, in each iteration we save the number of people in each type of state
llista_S=[] 
llista_A=[]
llista_I=[]
llista_R=[]
llista_D=[]
time=[]
cont_time=0

class A_15(GeoAgent):
    
  def __init__(self, unique_id, model, shape, probs=None):
    super().__init__(unique_id, model, shape)
    self.cont = 15
    self.displ = 3.5
    self.group=1
    self.state= 'S'
    self.substate= 'none'
    self.coordinates= [0,0]
    self.dni= 0
    self.provincia=''#Barcelona
    self.time_infection= 0  #latency time of infection I
    self.time_asymptomatic= 0 #latency time of asymptomatic
    self.time_death=0 #days before death
    self.commorbidity=0 #0:no commorbidity, 1:commorbidity
    self.mobility= 0
    self.go = 'In'
    self.prob_severe= 0.22 #rate per thousand
    self.prob_fatality= 0.5 #mortality risk


  def step(self):
    _log.debug("*** Agent %d stepping"%self.unique_id) 



class A_15_45(GeoAgent):

  def __init__(self, unique_id, model, shape, probs=None):
    super().__init__(unique_id, model, shape)
    self.cont = 22
    self.displ = 3.9
    self.group=2
    self.state= 'S'
    self.substate= 'none'
    self.coordinates= [0,0]
    self.dni= 0
    self.provincia=''#Barcelona
    self.time_infection=0 #num of infectious people
    self.time_asymptomatic=0
    self.time_death=0
    self.commorbidity=0 
    self.mobility= 0
    self.go = 'In'
    self.prob_severe= 1.85 #rate per thousand
    self.prob_fatality= 0.454148472


  def step(self):
    _log.debug("*** Agent %d stepping"%self.unique_id) 



class A_45_65(GeoAgent):

  def __init__(self, unique_id, model, shape, probs=None):
    super().__init__(unique_id, model, shape)
    self.cont = 17
    self.displ= 3.9
    self.group=3
    self.state= 'S'
    self.substate= 'none'
    self.coordinates= [0,0]
    self.dni= 0
    self.provincia=''#Barcelona
    self.time_infection=0
    self.time_asymptomatic=0
    self.time_death=0
    self.commorbidity=0 
    self.mobility= 0
    self.go = 'In'
    self.prob_severe= 4.98 #rate per thousand
    self.prob_fatality = 0.799357945


  def step(self):
    _log.debug("*** Agent %d stepping"%self.unique_id)



class A_65_74(GeoAgent):

  def __init__(self, unique_id, model, shape, probs=None):
    super().__init__(unique_id, model, shape)
    self.cont = 13
    self.displ = 3.55
    self.group=4
    self.state= 'S'
    self.substate= 'none'
    self.coordinates= [0,0]
    self.dni= 0
    self.provincia=''#Barcelona
    self.time_infection=0 #num of infectious people
    self.time_asymptomatic=0
    self.time_death=0
    self.commorbidity=0 
    self.mobility= 0
    self.go = 'In'
    self.prob_severe= 9.28 #rate per thousand
    self.prob_fatality= 0.924202228

  def step(self):
    _log.debug("*** Agent %d stepping"%self.unique_id)  



class A_74(GeoAgent):

  def __init__(self, unique_id, model, shape, probs=None):
    super().__init__(unique_id, model, shape)
    self.cont= 12
    self.displ = 3
    self.group=5
    self.state= 'S'
    self.substate= 'none'
    self.coordinates= [0,0]
    self.dni= 0
    self.provincia=''#Barcelona
    self.time_infection=0 #num of infectious people
    self.time_asymptomatic=0
    self.time_death=0
    self.commorbidity=0 
    self.mobility= 0
    self.go = 'In'
    self.prob_severe= 19.46 #rate per thousand
    self.prob_fatality = 0.963019547



  def step(self):
    _log.debug("*** Agent %d stepping"%self.unique_id)  
    #self.move()



class Bcn(Model): #model creation from Mesa

    def __init__(self,N,basemap):
        self._basemap= basemap #initialization
        self.grid = GeoSpace()
        self.schedule = RandomActivation(self)
        self.loadShapefiles()
        
        self.N_B = round(0.74*N)
        print(self.N_B)
        self.N_L= round(0.06*N)
        print(self.N_L)
        self.N_G = round(0.1* N)
        print(self.N_G)
        self.N_T = round(0.1*N)
        print(self.N_T)

        print('CREATION OF AGENTS OF BARCELONA')
        self.createAgents(self.N_B, 'B', 0)

        print('CREATION OF AGENTS OF LLEIDA')
        num_l = self.N_B -1 
        self.createAgents(self.N_L, 'L', num_l)

        num_g= (self.N_B + self.N_L) 
        print('CREATION OF AGENTS OF GIRONA')
        self.createAgents(self.N_G,'G', num_g)

        num_t= (self.N_B + self.N_L + self.N_T) 
        print('CREATION OF AGENTS OF TARRAGONA')
        self.createAgents(self.N_T, 'T', num_t)

        self.plotAll()  

        self.run_model(N)



    def loadShapefiles(self):
        self._loc = ctx.Place(self._basemap, zoom_adjust=0)  # zoom_adjust modifies the auto-zoom
        # Print some metadata
        self._xs = {}


        # Longitude w,e Latitude n,s

        for attr in ["w", "s", "e", "n", "place", "zoom", "n_tiles"]:
          self._xs[attr] = getattr(self._loc, attr)
          print("{}: {}".format(attr, self._xs[attr]))


        self._xs["centroid"] = LineString(
            (
              (self._xs["w"], self._xs["s"]),
              (self._xs["e"], self._xs["n"])
            )
          ).centroid

        self._xs["dx"] = 111.32; 
        self._xs["dy"] = 40075 * math.cos( self._xs["centroid"].y ) / 360

        _log.info("Arc amplitude at this latitude %f, %f"%(self._xs["dx"], self._xs["dy"]))

        path = os.getcwd()

        _log.info("Loading geo data from path:"+path)

        #roads_1 = gpd.read_file(os.path.join(path, "shapefiles","1","roads-line.shp"))
        #roads_2 = gpd.read_file(os.path.join(path, "shapefiles","2","buildings-polygon.shp"))
        limits = gpd.read_file(os.path.join(path, "proves-shapefiles","shapefiles","limits-administratius","geo_export_89e564f7-03e1-45d9-9c02-7f5e764610f2.shp"))
        self._limits = limits
        print('LIMITS:',self._limits.bounds)
        #self._roads = [roads_1, roads_2]



    def createAgents(self, N, prov, num):
        _h = """
        { 
          "type": "FeatureCollection",
          "crs": { 
            "type": "name", 
            "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } 
          },
          "features": []
        }
        """

        self._ioH1 = StringIO(_h)#converted to str
        self._ioH2 = StringIO(_h)
        self._ioH3 = StringIO(_h)
        self._ioH4 = StringIO(_h)
        self._ioH5 = StringIO(_h)
        self._ioH = StringIO(_h)


        #object creator 
        self._jsH1 = json.load(self._ioH1) 
        self._jsH2 = json.load(self._ioH2)
        self._jsH3 = json.load(self._ioH3)
        self._jsH4 = json.load(self._ioH4)
        self._jsH5 = json.load(self._ioH5)
        self._jsH  = json.load(self._ioH)


        self._ag0 = """
              { "type": "Feature", "id": 0, "properties": {}, 
                "geometry": { "type": "Point", 
                  "coordinates": [] 
                } 
              }"""

        if prov == 'B':
            self.prob_n=[0.1706, 0.3575, 0.2837, 0.0952, 0.093]
            min_x= min_x_B
            max_x= max_x_B
            min_y= min_y_B
            max_y = max_y_B

        
        if prov == 'T':
            self.prob_n= [0.1557, 0.3432, 0.2836, 0.1245, 0.093]
            min_x= min_x_T
            max_x= max_x_T
            min_y= min_y_T
            max_y = max_y_T


        if prov == 'L':
            self.prob_n= [0.1530, 0.3545, 0.2920, 0.1075, 0.093]
            min_x= min_x_L
            max_x= max_x_L
            min_y= min_y_L
            max_y = max_y_L

        
        if prov == 'G':
            self.prob_n= [0.1670, 0.3488, 0.2767, 0.1145, 0.093]
            min_x= min_x_G
            max_x= max_x_G
            min_y= min_y_G
            max_y = max_y_G


        self.N_15= round(self.prob_n[0]*N) 
        self.N_15_45= round(self.prob_n[1]*N)
        self.N_45_65= round(self.prob_n[2]*N) 
        self.N_65_74= round(self.prob_n[3]*N) 
        created = self.N_15 + self.N_15_45 + self.N_45_65 + self.N_65_74
        self.N_74= N - created

        self.dic_agents_15={} 
        self.dic_agents_15_45={}
        self.dic_agents_45_65={}
        self.dic_agents_65_74={}
        self.dic_agents_74={}

        base = self._xs["centroid"]


       
        dni1=[]
        coordinates1=[]
        if self.N_15 != 0:
            for a in range(self.N_15):
                a1 = a + num
                _ioAg1 = StringIO(self._ag0)
                self._jsAg1 = json.load(_ioAg1)
                self._jsAg1["id"] = a1 #Adding number of agent
                self._jsAg1["geometry"]["coordinates"] = [
                  random.uniform(min_x,max_x), 
                  random.uniform(min_y,max_y)]
                self._jsH1["features"].append( self._jsAg1)

                coordinates1.append(self._jsAg1["geometry"]["coordinates"])
                dni1.append(self._jsAg1["id"])

            AC_15=AgentCreator(A_15,{"model": self}) #AgentCreator, class of Mesa
            agents_15 = AC_15.from_GeoJSON(self._jsH1)



            for p,class_agent in enumerate(agents_15):
                class_agent.dni= dni1[p]
                print(class_agent.dni)
                class_agent.coordinates= coordinates1[p]
                class_agent.provincia = prov
            
            _log.info("Adding %d agents..."%len(agents_15))

            self.grid.add_agents(agents_15)


            for agent in agents_15:
                self.schedule.add(agent) 
        else: 
            a1= num


        dni2=[]
        coordinates2=[]
        if self.N_15_45 != 0:

            for b in range(self.N_15_45):
                b1 = a1 + b + 1
                _ioAg2 = StringIO(self._ag0)
                self._jsAg2 = json.load(_ioAg2)
                self._jsAg2["id"] = b1 #Adding number of agent
                self._jsAg2["geometry"]["coordinates"] = [
                  random.uniform(min_x,max_x), 
                  random.uniform(min_y,max_y)]
                self._jsH2["features"].append(self._jsAg2 )

                coordinates2.append(self._jsAg2["geometry"]["coordinates"])
                dni2.append(self._jsAg2["id"])


            AC_15_45=AgentCreator(A_15_45,{"model": self}) #AgentCreator, class of Mesa
            agents_15_45 = AC_15_45.from_GeoJSON(self._jsH2)


            _log.info("Adding %d agents..."%len(agents_15_45))

            self.grid.add_agents(agents_15_45)



            for agent in agents_15_45:
                self.schedule.add(agent) 


            for p,class_agent in enumerate(agents_15_45):
                class_agent.dni= dni2[p]
                print(class_agent.dni)
                class_agent.coordinates= coordinates2[p]
                class_agent.provincia = prov
        elif a1==0: 
            b1= num
        else: 
            b1 = a1
        
        dni3=[]
        coordinates3=[]
        if self.N_45_65 != 0:

            for c in range(self.N_45_65):
                c1 = b1 + c + 1
                _ioAg3 = StringIO(self._ag0)
                self._jsAg3 = json.load(_ioAg3)
                self._jsAg3["id"] = c1 #Adding number of agent
                self._jsAg3["geometry"]["coordinates"] = [
                  random.uniform(min_x, max_x), 
                  random.uniform(min_y,max_y)]
                self._jsH3["features"].append( self._jsAg3 )


                coordinates3.append(self._jsAg3["geometry"]["coordinates"])
                dni3.append(self._jsAg3["id"])


            AC_45_65=AgentCreator(A_45_65,{"model": self}) #AgentCreator, class of Mesa
            agents_45_65 = AC_45_65.from_GeoJSON(self._jsH3)


            _log.info("Adding %d agents..."%len(agents_45_65))

            self.grid.add_agents(agents_45_65)


            for agent in agents_45_65:
                self.schedule.add(agent) 


            for p,class_agent in enumerate(agents_45_65):
                class_agent.dni= dni3[p]
                print(class_agent.dni)
                class_agent.coordinates= coordinates3[p]
                class_agent.provincia = prov
        
        elif b1==0: 
            c1= num
        else: 
            c1 = b1


        dni4=[]
        coordinates4=[]
        if self.N_65_74 != 0: 

            for d in range(self.N_65_74):
                d1 = c1 + d + 1
                _ioAg4 = StringIO(self._ag0)
                self._jsAg4 = json.load(_ioAg4)
                self._jsAg4["id"] = d1#Adding number of agent
                self._jsAg4["geometry"]["coordinates"] = [
                  random.uniform(min_x,max_x), 
                  random.uniform(min_y,max_y)]
                self._jsH4["features"].append( self._jsAg4 )

                coordinates4.append(self._jsAg4["geometry"]["coordinates"])
                dni4.append(self._jsAg4["id"])


            AC_65_74=AgentCreator(A_65_74,{"model": self}) #AgentCreator, class of Mesa
            agents_65_74 = AC_65_74.from_GeoJSON(self._jsH4)


            _log.info("Adding %d agents..."%len(agents_65_74))

            self.grid.add_agents(agents_65_74)


            for agent in agents_65_74:
                self.schedule.add(agent) 


            for p,class_agent in enumerate(agents_65_74):
                class_agent.dni= dni4[p]
                print(class_agent.dni)
                class_agent.coordinates= coordinates4[p]
                class_agent.provincia = prov
        elif c1==0: 
            d1= num
        else: 
            d1 = c1



        dni5=[]
        coordinates5=[]
        if self.N_74 != 0: 

            for e in range(self.N_74):
                e1 = d1 + e + 1
                _ioAg5 = StringIO(self._ag0)
                self._jsAg5 = json.load(_ioAg5)
                self._jsAg5["id"] = e1 #Adding number of agent
                self._jsAg5["geometry"]["coordinates"] = [
                  random.uniform(min_x,max_x), 
                  random.uniform(min_y,max_y)]
                self._jsH5["features"].append(self._jsAg5 )


                coordinates5.append(self._jsAg5["geometry"]["coordinates"])
                dni5.append(self._jsAg5["id"])


            AC_74=AgentCreator(A_74,{"model": self}) #AgentCreator, class of Mesa
            agents_74 = AC_74.from_GeoJSON(self._jsH5)


            _log.info("Adding %d agents..."%len(agents_74))

            self.grid.add_agents(agents_74)


            for agent in agents_74:
                self.schedule.add(agent) 


            for p,class_agent in enumerate(agents_74):
                class_agent.dni= dni5[p]
                print(class_agent.dni)
                class_agent.coordinates= coordinates5[p]
                class_agent.provincia = prov
            
        elif d1==0: 
            e1= num
        else: 
            e1 = d1




    def plotAll(self):

        fig = plt.figure(figsize=(15, 15))
        ax1 = plt.gca()
        ctx.plot_map(self._loc, ax=ax1)
        self._limits.plot(ax=ax1, facecolor='none', edgecolor='black') 


        """
        _c = ["red", "blue"]
        
        for i, _r in enumerate(self._roads):

          _r.plot(ax=ax1, facecolor='none', edgecolor=_c[i])    

        """



        # Plot agents

        agentFeatures = self.grid.__geo_interface__
        #print(agentFeatures)
        gdf = gpd.GeoDataFrame.from_features(agentFeatures)


        group1=gdf[gdf.group==1]
        group1.plot(ax=ax1,color='black')

        group2=gdf[gdf.group==2]
        group2.plot(ax=ax1,color='blue')

        group3=gdf[gdf.group==3]
        group3.plot(ax=ax1,color='red')

        group4=gdf[gdf.group==4]
        group4.plot(ax=ax1,color='green')

        group5=gdf[gdf.group==5]
        group5.plot(ax=ax1,color='yellow')


        agent=self.grid.agents[19]
        neighbors = self.grid.get_neighbors(agent)


        for a in neighbors:
            neig=gdf[gdf.dni==a.dni]
            neig.plot(ax=ax1,color='orange')





    def infectionA(self, agent):
        '''Function that with a probability called virus_spread_chance a susceptible 
        becomes asymptomatic. An infected person can infect a maximum number of people 
        controlled by the number of contacts variable (self.cont). 
        Once a neighbor is infected, we assign a latency period varible, that 
        represents the days that a  person will remain asymptomatic.
        This function is used for asymptotics and infected people, since both have 
        the possibility to infect the others'''

        virus_spread_chance = 0.06 #probability to infect
        identity = agent.loc['dni',] #get variable dni from the agent
        contacts = agent.loc['cont',] #get variable nº of contacts from the agent

        agent_infection=self.grid.agents[identity] #select the agent
        neighbors_i= self.grid.get_neighbors(agent_infection) #select the neighbors of the agent
        iter_neighbors=len(neighbors_i) #number of neighbors
        cont=0

        for a in (neighbors_i):
            #For each neighbor 
            if contacts >= cont:         
                if iter_neighbors > contacts:
                #If the number of neighbors exceeds the maximum nº of contacts, it is needed a restriction
                    probability_infection= random.uniform(0,1) #new probability of get infected for each neighbor
                    if probability_infection<= virus_spread_chance:
                        if a.state == 'S': 
                            a.state='A'
                            a.time_asymptomatic=int(random.normalvariate(progression_period_asym,progression_sd_asym))

                else:
                    probability_infection= random.uniform(0,1)
                    if probability_infection<= virus_spread_chance:
                        if a.state == 'S': 
                            a.state='A'
                            a.time_asymptomatic=int(random.normalvariate(progression_period_asym,progression_sd_asym))

            cont= cont+1

            
    def symptomA(self,agent):
        '''This function looks if an asymptomatic agent has passed the period 
        of asymptomatic latency. When this period ends, the agent develops symptoms 
        and changes its status to infected. 
        A substate is also added to determine the severity of the infection: mild 
        or severe. '''

        #prob_mild = 0.15 
        identity = agent.loc['dni',]
        agent=self.grid.agents[identity]
        time=agent.time_asymptomatic
        prob_mild = 1 - agent.prob_severe

        if time < 1:
            agent.state='I'
            
            #Severity of the infection 
            prob_severity= random.uniform(0,1)
            #print('probability:', prob_severity)
            if prob_severity >= prob_mild:
                agent.substate='mild'
                #print('SUBSTATE', agent.substate)
                agent.time_infection=int(random.normalvariate(progression_period,progression_sd))
            else:
                agent.substate='severe'

        else:
            time = time - 1 
            agent.time_asymptomatic = time
            #print(agent.time_infection)

            

        

    def recovery(self,agent):
        '''This function changes the infected agent state to a recovery state,
        when the latency time of infection ends and the severity is mild or the
        time_death variable is zero.''' 


        identity = agent.loc['dni',]
        agent=self.grid.agents[identity]
        time=agent.time_infection


        if time < 1 and agent.state == 'I':
            #print('estat inicial', agent.state)
            agent.state='R'
            agent.substate='none'
            #print('estat final',agent.state)
            #print('estat final',agent.substate)

        else:
            time = time - 1 
            agent.time_infection = time

            

    def hospitalization(self, agent):
        '''When an infected person has a severe substate a death probability is 
        calculated. If this probability is lower than the fatality probability, 
        the agent will die (in  a period called time_death). If not, the  period 
        of infection will increase some days (plus_time). ''' 
        

        #prob_fatality=0.025
        identity = agent.loc['dni',]
        agent=self.grid.agents[identity]
        prob_fatality = agent.prob_fatality
        prob_death= random.uniform(0,1)
        #print('prob_death:', prob_death)


        if prob_death <= prob_fatality:
            agent.time_death=int(random.normalvariate(progression_period,progression_sd))
            #print('MORT')

        else:
            plus_time=int(random.normalvariate(progression_period,progression_sd))
            agent.time_infection = agent.time_infection + plus_time
            #print('PLUS TIME')

                



    def death(self, agent):
        '''Agents who enter this function, change its state to dead when the 
        latency period (time_death) ends.''' 

        identity = agent.loc['dni',]
        agent=self.grid.agents[identity]
        time=agent.time_death

        if time < 1 and agent.state == 'I':
            #print('estat inicial', agent.state)
            agent.state='D'
            agent.substate='none'
            #print('estat final',agent.state)
            #print('estat final',agent.substate)

        else:
            time = time - 1 
            agent.time_death = time

                



    def initialization(self, N_B):
        '''We take x agents from the population (randomly) and we change their 
        status to asymptomatic. These agents are the agent 0, the ones who bring 
        the covid to the population. The number of agent 0 is controlled by the
        variable initial_outbreak_size. '''


        initial_outbreak_size= 2 #nº of agents 0

        for i in range(0,initial_outbreak_size):
            num_agent_0=int(random.uniform(0,N_B))        
            agent_0=self.grid.agents[num_agent_0]
            #print('initial state', agent_0.state)
            agent_0.state= 'A'
            #print('Agent_0 dni',agent_0.dni)
            #print('final', agent_0.state)
            
            #We add a latency time to the agent 0
            agent_0.time_asymptomatic=int(random.normalvariate(progression_period_asym,progression_sd_asym)) 
            
            
    def prob_movement(self, Provincia):
        agentFeatures = self.grid.__geo_interface__
        gdf = gpd.GeoDataFrame.from_features(agentFeatures)

        if Provincia == 'Barcelona':
            group=gdf[gdf.provincia== 'B']
            shape=group.shape

            dni1=[]

            for p in range(0,shape[0]):
                agent=group.iloc[p]
                a_identity = agent.loc['dni',]
                dni1.append(a_identity)
            agents_out= round(B_OUT*shape[0])
            print('NUMERO:',agents_out)

            
            for k in range(0,agents_out):
                num_agent=int(random.uniform(0,shape[0])) 
                chosen= dni1[num_agent]
                print(chosen)
                agent= self.grid.agents[chosen]
                prob= random.uniform(0,1)

                if prob<= B_L: 
                    agent.go= 'Lleida'
                elif prob> B_L and prob <= (B_L+B_G): 
                    agent.go= 'Girona'
                else: 
                    agent.go= 'Tarragona'

                    
        if Provincia == 'Lleida':
            group=gdf[gdf.provincia== 'L']
            shape=group.shape

            dni2=[]

            for p in range(0,shape[0]):
                agent=group.iloc[p]
                a_identity = agent.loc['dni',]
                dni2.append(a_identity)
            agents_out= round(L_OUT*shape[0])
            print('NUMERO:',agents_out)

            
            for k in range(0,agents_out):
                num_agent=int(random.uniform(0,shape[0])) 
                chosen= dni2[num_agent]
                print(chosen)
                agent= self.grid.agents[chosen]
                prob= random.uniform(0,1)


                if prob<= L_G: 
                    agent.go= 'Girona'
                elif prob> L_G and prob <= (L_G+L_B): 
                    agent.go= 'Barcelona'
                else: 
                    agent.go= 'Tarragona'

            
        if Provincia == 'Girona':
            group=gdf[gdf.provincia== 'G']
            shape=group.shape

            dni3=[]
            for p in range(0,shape[0]):
                agent=group.iloc[p]
                a_identity = agent.loc['dni',]
                dni3.append(a_identity)
            agents_out= round(G_OUT*shape[0])
            print('NUMERO:',agents_out)

            
            for k in range(0,agents_out):
                num_agent=int(random.uniform(0,shape[0])) 
                chosen= dni3[num_agent]
                print(chosen)
                agent= self.grid.agents[chosen]
                prob= random.uniform(0,1)

            
                if prob<= G_L: 
                    agent.go= 'Lleida'
                elif prob> G_L and prob <= (G_L+G_B): 
                    agent.go= 'Barcelona'
                else: 
                    agent.go= 'Tarragona'


        if Provincia == 'Tarragona':
            group=gdf[gdf.provincia== 'T']
            shape=group.shape

            dni4=[]

            for p in range(0,shape[0]):
                agent=group.iloc[p]
                a_identity = agent.loc['dni',]
                dni4.append(a_identity)
            agents_out= round(T_OUT*shape[0])
            print('NUMERO:',agents_out)


            for k in range(0,agents_out):
                num_agent=int(random.uniform(0,shape[0])) 
                chosen= dni4[num_agent]
                print(chosen)
                agent= self.grid.agents[chosen]
                prob= random.uniform(0,1)

                if prob<= T_L: 
                    agent.go= 'Lleida'
                elif prob> T_L and prob <= (T_L+T_B): 
                    agent.go= 'Barcelona'
                else: 
                    agent.go= 'Girona'
                    
                    


    def move(self, num_agent):
        #No es pot canviar la localitzacio, cal destruir agent i crear-ne un de nou. 
        a= self.grid.agents[num_agent]
        coord = a.coordinates
        state= a.state
        commor= a.commorbidity
        infection= a.time_infection
        asym = a.time_asymptomatic
        death = a.time_death
        substate = a.substate
        where = a.go
        severe= a.prob_severe
        fatality = a.prob_fatality
        print(coord)
        dni= a.dni
        print('DNI:',dni)
        age = a.group

        if age == 1:
            new= A_15
        if age == 2:
            new= A_15_45
        if age == 3:
            new= A_45_65
        if age == 4:
            new= A_65_74
        if age == 5:
            new= A_74
        print(len(self.grid.agents))
        del self.grid.agents[num_agent] #delete a particular agent
        print(len(self.grid.agents))

        
        #dins de la provincia: 'In'
        if where == 'In':
            self.mobility_range= 0.2 #mobility range inside to 
            dx= random.uniform(-self.mobility_range, self.mobility_range)
            dy= random.uniform(-self.mobility_range, self.mobility_range)
            coord_x= coord[0]+dx
            coord_y= coord[1]+dy
            print('noves coordenades')
            print(coord_x,coord_y )

        if where == 'Girona':
            #limits de Girona
            coord_x= random.uniform(min_x_G, max_x_G)
            coord_y= random.uniform(min_y_G, max_y_G)

        if where == 'Tarragona':
            #limits de Tarragona
            coord_x= random.uniform(min_x_T, max_x_T)
            coord_y= random.uniform(min_y_T, max_y_T)

        if where == 'Lleida ':
            #limits de Lleida 
            coord_x= random.uniform(min_x_L, max_x_L)
            coord_y= random.uniform(min_y_L, max_y_L)


        if where == 'Barcelona':
            #limits de Barcelona
            coord_x= random.uniform(min_x_B, max_x_B)
            coord_y= random.uniform(min_y_B, max_y_B)


        #Creation of the new agent
        for a in range(1):
            self._jsH["features"]= []
            _ioAg = StringIO(self._ag0)
            self._jsAg = json.load(_ioAg)
            self._jsAg["id"] = a #Adding number of agent
            self._jsAg["geometry"]["coordinates"] = [coord_x, coord_y ]
            self._jsH["features"].append(self._jsAg)

            AC = AgentCreator(new, {"model": self})
            print(AC)
            agent = AC.from_GeoJSON(self._jsH)
            print(agent)

            for i in agent: 
                i.dni= dni
                i.coordinates = self._jsAg["geometry"]["coordinates"]
                i.state = state
                i.commorbidity = commor
                i.mobility = 1
                i.time_infection = infection
                i.time_death = death 
                i.time_asymptomatic = asym
                i.substate = substate
                i.prob_severe= severe
                i.prob_fatality = fatality

                print('dni')
                print(i.dni)

            self.grid.add_agents(agent)
        print(len(self.grid.agents))




    def cont_states(self,gdf):
        '''Count in each iteration the number of S, I...'''
        
        #Number of susceptible
        group_S=gdf[gdf.state=='S']
        num_S= group_S.shape[0]
        llista_S.append(num_S)
        print('susceptible number', llista_S)

        #Number of asymptomatic
        group_A=gdf[gdf.state=='A']
        num_A= group_A.shape[0]
        llista_A.append(num_A)
        print('asymptomatic number', llista_A)

        #Number of infected
        group_I=gdf[gdf.state=='I']
        num_I= group_I.shape[0]
        llista_I.append(num_I)
        print('infected number', llista_I)

        #Number of recovered
        group_R=gdf[gdf.state=='R']
        num_R= group_R.shape[0]
        llista_R.append(num_R)
        print('recovered number', llista_R)

        #Number of death
        group_D=gdf[gdf.state=='D']
        num_D= group_D.shape[0]
        llista_D.append(num_D)
        print('death number', llista_D)
        


    def step(self,i,N):
        self.schedule.step()  #advance on step
        
        '''In the first step, initialization function is done. We "add" the 
        agents 0 to the susceptible population'''
        if i == 1:
            print('INITIALIZATION')
            self.initialization(N) 

        
        agentFeatures = self.grid.__geo_interface__
        gdf = gpd.GeoDataFrame.from_features(agentFeatures)
        
        infected = gdf[gdf.state == 'I']  #all the infected
        asymptomatic = gdf[gdf.state == 'A'] #all the asymptomatic
        shapeA=asymptomatic.shape #dimensions of the variable
        shapeI=infected.shape 

        
        #For each asymptomatic: 
        for n in range (0,shapeA[0]): 
            a=asymptomatic.iloc[n] #take each row
            self.infectionA(a) #try to infect
            self.symptomA(a) #some will develop symptoms 


             
        #For each infected:
        for k in range (0,shapeI[0]): 
            i_agent=infected.iloc[k] #each row
            self.infectionA(i_agent) #try to infect
            
            identity = i_agent.loc['dni',]
            agent=self.grid.agents[identity]

            #Depending on the infection severity 
            if agent.substate == 'mild':
                self.recovery(i_agent)

            else:
                self.hospitalization(i_agent)
                if agent.time_death > 0:
                    self.death(i_agent) 
                else:
                    self.recovery(i_agent)


        #To count the number of people in each type of state
        self.cont_states(gdf)


    def map_epidem(self):
        fig = plt.figure(figsize=(15, 15))
        ax1 = plt.gca()
        ctx.plot_map(self._loc, ax=ax1)
        self._limits.plot(ax=ax1, facecolor='none', edgecolor='black') 


        """
        _c = ["red", "blue"]
        
        for i, _r in enumerate(self._roads):

          _r.plot(ax=ax1, facecolor='none', edgecolor=_c[i])    

        """



        # Plot agents

        agentFeatures = self.grid.__geo_interface__
        #print(agentFeatures)
        gdf = gpd.GeoDataFrame.from_features(agentFeatures)


        group1=gdf[gdf.state== 'S']
        group1.plot(ax=ax1,color='green')

        group2=gdf[gdf.state=='A']
        group2.plot(ax=ax1,color='magenta')

        group3=gdf[gdf.state=='I']
        group3.plot(ax=ax1,color='red')

        group4=gdf[gdf.state=='R']
        group4.plot(ax=ax1,color='blue')

        group5=gdf[gdf.state=='D']
        group5.plot(ax=ax1,color='black')

        
        

    def run_model(self, n):
        self.prob_movement('Barcelona') 
        self.prob_movement('Girona') 
        self.prob_movement('Lleida') 
        self.prob_movement('Tarragona') 


        #First move
        for i in range(n):
            agentFeatures = self.grid.__geo_interface__
            gdf = gpd.GeoDataFrame.from_features(agentFeatures)
            no_move = gdf[gdf.mobility == 0]#agents que encara no s'han mogut 

            for p in range (0,1): 
                a= no_move.iloc[p]
                dni= a.loc['dni',]

                self.move(dni)

            agentFeatures = self.grid.__geo_interface__
            gdf = gpd.GeoDataFrame.from_features(agentFeatures)
            print(gdf)

                
        #Second, see the SAIRD MODEL
        for i in range(n-1):

            _log.info("Step %d of %d"%(i, n))

            self.step(i,n)
            
            self.map_epidem()


        #dim=llista_S.shape
        time=np.linspace(1,41,41)
        time=time.transpose()
        
        plt.figure()
        #plt.hold(True)
        plt.plot(time,llista_S, 'g-')
        plt.plot(time,llista_A, 'm-')
        plt.plot(time,llista_I, 'r-')
        plt.plot(time,llista_R, 'b-')
        plt.plot(time,llista_D, 'k-')




"""

if __name__ == '__main__':

    A=Bcn(35)

"""      



   



            



        



        



    



    



    